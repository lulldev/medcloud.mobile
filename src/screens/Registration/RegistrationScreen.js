import React from 'react';
import { View } from 'react-native';
import { Card, Input, Button, Text } from 'react-native-elements';

class RegistratinScreen extends React.Component {
  render() {
    return (
      <View style={{
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        alignContent: 'center',
      }}>
        <Card>
          <Text style={{
            alignSelf: 'center',
            fontSize: 18,
            marginTop: 25,
            marginBottom: 25,
          }}>
            {'Создать учетную запись'.toUpperCase()}
          </Text>
          <View style={{
              marginBottom: 25,
            }}>
            <Input placeholder='+ 7 (___) ___ __ __'/>
          </View>
          <View style={{
              marginBottom: 25,
            }}>
            <Input placeholder="Пароль" />
          </View>
          <Button
            title="Продолжить"
            style={{width: '90%', alignSelf: 'center', marginBottom: 25}}
          />
        </Card>
      </View>
    );
  }
}

export default RegistratinScreen;