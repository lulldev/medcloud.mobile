import React from 'react';
import {
  View,
  ImageBackground,
  Image,
} from 'react-native';
import { Button } from 'react-native-elements';

class StartScreen extends React.Component {
  render() {
    return (
      <View style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'stretch',
      }}>
        <View style={{height: '50%'}}>
          <ImageBackground source={require('./bg.png')} style={{width: '100%', height: '100%' }}>
            <Image source={require('./logo.png')} style={{top: 25, left: 25}} />
          </ImageBackground>
        </View>
        <View style={{height: '50%', alignContent: 'center', top: 30, alignItems: 'stretch'}}>
          <View style={{justifyContent: 'center'}}>
            <Button
              title="Регистрация"
              onPress={() => this.props.navigation.navigate('Registration')}
              style={{width: '90%', alignSelf: 'center', marginBottom: 20}}
            />
          </View>
          <Button
            title="Вход"
            type="outline"
            onPress={() => this.props.navigation.navigate('Profile')}
            style={{width: '90%', alignSelf: 'center', marginBottom: 20}}
          />
          <Button
            title="Забыли пароль"
            type="clear"
            style={{width: '90%', alignSelf: 'center'}}
          />
        </View>
      </View>
    );
  }
}

export default StartScreen;