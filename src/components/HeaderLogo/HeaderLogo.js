import React from 'react';
import { Image } from 'react-native';

class HeaderLogo extends React.Component {
  render() {
    return (
      <Image
        source={require('./logo.png')}
        style={{ width: 92, height: 35 }}
      />
    );
  }
}

export default HeaderLogo;
