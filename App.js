import React from 'react';
import {
  createStackNavigator,
  createAppContainer,
} from 'react-navigation';
import { StartScreen } from './src/screens/Start';
import { RegistrationScreen } from './src/screens/Registration';
import { HeaderLogo } from './src/components/HeaderLogo';
import { Icon } from 'react-native-elements';

const AppNavigator = createStackNavigator(
  {
    Start: {
      screen: StartScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
    Registration: {
      screen: RegistrationScreen,
      navigationOptions: ({ navigation }) => ({
        headerTitle: <HeaderLogo />,
        headerLeft: <Icon name={'chevron-left'}
          onPress={ () => { navigation.goBack() }} />,
      }),
    },
  },
  {
    initialRouteName: 'Start'
  }
);

export default createAppContainer(AppNavigator);